import Head from 'next/head';
import React, { useEffect } from 'react';
import Web3 from 'web3';
import { CONTRACT_APP_ADDRESS, CONTRACT_APP_ABI, renderBalance, renderTo, renderMetaMaskInstall } from '../utils/utils';
import Link from 'next/link'

export default function Home({ data }) {

  useEffect(async () => {

    const web3 = new Web3(window.ethereum);
    web3.eth.getAccounts(function (err, accounts) {
      if (err != null) console.error("An error occurred: " + err);
      else if (accounts.length == 0) {
        const node = renderBalance('', -1);
        renderTo(node, 'wallet');
        const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: CONTRACT_APP_ADDRESS });
        appContract.methods.getMatch(true).call(function (err, res) {
          if (err) {
            console.log("An error occured", err)
            return
          }
          const muccuoc = {};
          if (res.length > 0) {
            const lichthidau = [];
            for (const match of res) {
              lichthidau.push({ 'id': match[0], 'team1': match[4], 'team2': match[5], 'tengiaidau': match[3], 'vongdau': match[2], 'startDate': parseInt(match[1]), 'allowToBet': match[7], 'mode': parseInt(match[10]) });
              muccuoc[match[0]] = web3.utils.fromWei(match[8], 'ether');
            }
            sessionStorage.setItem('muccuoc', JSON.stringify(muccuoc));
            let scheduleNode = renderLichThiDau(lichthidau, CONTRACT_APP_ADDRESS);
            renderTo(scheduleNode, 'schedule');
          }
        });
      }
      else {
        const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
        appContract.methods.balanceOf(accounts[0]).call(function (err, res) {
          if (err) {
            console.log("An error occured", err)
            return
          }
          const node = renderBalance(accounts[0], web3.utils.fromWei(res, 'ether'));
          renderTo(node, 'wallet');
        });
        appContract.methods.getMatch(true).call(function (err, res) {
          if (err) {
            console.log("An error occured", err)
            return
          }
          const muccuoc = {};
          if (res.length > 0) {
            const lichthidau = [];
            for (const match of res) {
              lichthidau.push({ 'id': match[0], 'team1': match[4], 'team2': match[5], 'tengiaidau': match[3], 'vongdau': match[2], 'startDate': parseInt(match[1]), 'allowToBet': match[7], 'mode': parseInt(match[10]) });
              muccuoc[match[0]] = web3.utils.fromWei(match[8], 'ether');
            }
            sessionStorage.setItem('muccuoc', JSON.stringify(muccuoc));
            let scheduleNode = renderLichThiDau(lichthidau, accounts[0]);
            renderTo(scheduleNode, 'schedule');
          }
        });
      }
    });
    if (typeof window.ethereum !== 'undefined') {
      ethereum.on('accountsChanged', function (accounts) {
        if (accounts.length > 0) {
          const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
          appContract.methods.balanceOf(accounts[0]).call(function (err, res) {
            if (err) {
              console.log("An error occured", err)
              return
            }
            const node = renderBalance(accounts[0], web3.utils.fromWei(res, 'ether'));
            renderTo(node, 'wallet');
          });
        } else {
          const node = renderBalance('', -1);
          renderTo(node, 'wallet');
        }
      });
    } else {
      const node = renderMetaMaskInstall();
      renderTo(node, 'wallet');
    }

  });

  return (
    <div>
      <Head>
        <title>Lich thi dau</title>
        <meta name="description" content="Lich thi dau" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <div className='block'></div>
        <div className="columns is-mobile has-text-centered">
          <div className="column"></div>
          <div className="column is-10">
            <nav className="breadcrumb" aria-label="breadcrumbs">
              <ul>
                <li className="is-active"><Link href="/"><a>Home</a></Link></li>
                <li><Link href="/swap"><a>Swap</a></Link></li>
                <li><Link href="https://docs.binance.org/smart-chain/wallet/metamask.html" target="_blank"><a>Setup BSC Test Net</a></Link></li>
                <li><Link href="https://testnet.binance.org/faucet-smart" target="_blank"><a>Faucet</a></Link></li>
              </ul>
            </nav>
          </div>
          <div className="column"></div>
        </div>
        <div id='wallet'>
        </div>
        <div id='schedule'>
        </div>
        <div id="snackbar">Address coppied to clipboard!</div>
      </main>

      <footer>

      </footer>
    </div>
  )
}

export function renderLichThiDau(data, selectedAddress) {

  return (<div>
    {
      data.map((row) => {
        const id = row.id;
        const team1 = row.team1;
        const team2 = row.team2;
        const giaiDau = row.tengiaidau;
        const vongDau = row.vongdau;
        const startDate = new Date(row.startDate);
        const allowToBet = row.allowToBet;
        const strDate = ((startDate.getDate() > 9) ? startDate.getDate() : ('0' + startDate.getDate())) + '/' + ((startDate.getMonth() > 8) ? (startDate.getMonth() + 1) : ('0' + (startDate.getMonth() + 1))) + '/' + startDate.getFullYear();
        const strTime = ((startDate.getHours() > 9) ? startDate.getHours() : ('0' + startDate.getHours())) + ':' + ((startDate.getMinutes() > 9) ? startDate.getMinutes() : ('0' + startDate.getMinutes()));
        const tranDau = { 'id': id, 'team1': team1, 'team2': team2, 'tenGiaiDau': giaiDau, 'strDate': strDate, 'strTime': strTime, 'vongDau': vongDau, 'allowToBet': allowToBet, 'mode': row.mode };
        return renderTranDau(tranDau, selectedAddress);
      })
    }
  </div>)
}

export function renderTranDau(tranDau, selectedAddress) {

  const web3 = new Web3(window.ethereum);
  const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: selectedAddress });
  appContract.methods.getBet(tranDau.id).call(function (err, res) {
    if (err) {
      console.log("An error occured", err)
      return
    }
    let node = renderEnableBet(tranDau);
    if (res[1].toUpperCase() === selectedAddress.toUpperCase()) {
      node = renderDisableBet(tranDau, res[2]);
    }
    renderTo(node, tranDau.id + 'dudoan');
  });
  appContract.methods.getBetList(tranDau.id).call(function (err, res) {
    if (err) {
      console.log("An error occured", err)
      return
    }
  });
  return (<div key={tranDau.id}>
    <div className='block'></div>
    <div className="box match-width">
      {/* <h3 className='subtitle is-3'>19/2/2022</h3> */}
      <div className="columns is-mobile has-text-centered">
        <div className="column"><b>{tranDau.strDate}</b></div>
        <div className="column"><b>{tranDau.vongDau}</b></div>
        <div className="column"><b>{tranDau.tenGiaiDau}</b></div>
      </div>
      <hr></hr>
      <div className="columns is-mobile has-text-centered">
        <div className="column"><span className="tag is-primary is-light is-large">{tranDau.team1}</span></div>
        <div className="column">{tranDau.strTime}</div>
        <div className="column"><span className="tag is-danger is-light is-large">{tranDau.team2}</span></div>
      </div>
      <hr></hr>
      <div id={tranDau.id + 'dudoan'}>

      </div>
    </div>
  </div>)
}

const onClickBet = async (id, mode) => {

  const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
  if (accounts.length > 0) {
    const web3 = new Web3(window.ethereum);
    const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
    if (mode === 0) {
      const goal1 = document.getElementById(id + 'team1').value;
      const goal2 = document.getElementById(id + 'team2').value;
      const muccuoc = JSON.parse(sessionStorage.getItem('muccuoc'));
      const amount = muccuoc[id];
      appContract.methods.balanceOf(accounts[0]).call(function (err, res) {
        if (err) {
          console.log("An error occured", err)
          return
        }
        const lastestBalance = web3.utils.fromWei(res, 'ether');
        if (lastestBalance < parseFloat(amount)) {
          alert('Số dư của bạn không đủ đặt cược. Vui lòng swap token để tiếp tục');
          return;
        }
        const result = goal1 + '-' + goal2;
        appContract.methods
          .bet(id, result)
          .send({}, function (err, res) {
            if (err) {
              console.log("An error occured", err);
              return;
            }
          }).then(function (receipt) {
            // will be fired once the receipt is mined
            appContract.methods.balanceOf(accounts[0]).call(function (err, res) {
              if (err) {
                console.log("An error occured", err)
                return
              }
              const node = renderBalance(accounts[0], web3.utils.fromWei(res, 'ether'));
              renderTo(node, 'wallet');
              location.reload();
            });
          });
      });
    }

    if (mode === 1 || mode === 2) {
      const result = document.getElementById(id + 'mode').value;
      const muccuoc = JSON.parse(sessionStorage.getItem('muccuoc'));
      const amount = muccuoc[id];
      const balance = parseInt(sessionStorage.getItem('balance'));
      if (balance < amount) {
        alert('Số dư của bạn không đủ đặt cược. Vui lòng swap token để tiếp tục');
        return;
      }
      if (result !== "-1") {
        appContract.methods
          .bet(id, result)
          .send({}, function (err, res) {
            if (err) {
              console.log("An error occured", err);
              return;
            }
          }).then(function (receipt) {
            // will be fired once the receipt is mined
            appContract.methods.balanceOf(accounts[0]).call(function (err, res) {
              if (err) {
                console.log("An error occured", err)
                return
              }
              const node = renderBalance(accounts[0], web3.utils.fromWei(res, 'ether'));
              renderTo(node, 'wallet');
              location.reload();
            });
          });
      }
    }
  }
};


export function renderEnableBet(tranDau) {

  if (tranDau.mode == 0) {
    return renderEnableBetMode0(tranDau);
  }
  if (tranDau.mode == 1) {
    return renderEnableBetMode1(tranDau);
  }
  if (tranDau.mode == 2) {
    return renderEnableBetMode2(tranDau);
  }
}

export function renderEnableBetMode0(tranDau) {

  if (tranDau.allowToBet) {
    return (<div>
      <div className="columns is-mobile has-text-centered">
        <div className="column"></div>
        <div className="column"><h4 className='subtitle is-4'>Dự đoán</h4></div>
        <div className="column"></div>
      </div>
      <div className="columns is-mobile has-text-centered">
        <div className="column is-6"><span className="tag is-primary is-light is-large">{tranDau.team1}</span></div>
        <div className="column is-4"><input className="input block" type="number" placeholder="0" id={tranDau.id + 'team1'} defaultValue={0} min={0}></input></div>
        <div className="column"></div>
      </div>
      <div className="columns is-mobile has-text-centered">
        <div className="column is-6"><span className="tag is-danger is-light is-large">{tranDau.team2}</span></div>
        <div className="column is-4"><input className="input block" type="number" placeholder="0" id={tranDau.id + 'team2'} defaultValue={0} min={0}></input></div>
        <div className="column"></div>
      </div>
      <div className="columns is-mobile has-text-centered">
        <div className="column"></div>
        <div className="column"><button className="button is-link is-small" onClick={() => onClickBet(tranDau.id, tranDau.mode)}>Đặt cược</button></div>
        <div className="column"></div>
      </div>
    </div>)
  }
  return (<div>
    <div className="columns is-mobile has-text-centered">
      <div className="column"></div>
      <div className="column"><h4 className='subtitle is-4'>Dự đoán</h4></div>
      <div className="column"></div>
    </div>
    <div className="columns is-mobile has-text-centered">
      <div className="column is-6"><span className="tag is-primary is-light is-large">{tranDau.team1}</span></div>
      <div className="column is-4"><input className="input block" type="number" placeholder="0" id={tranDau.id + 'team1'} disabled={true}></input></div>
      <div className="column"></div>
    </div>
    <div className="columns is-mobile has-text-centered">
      <div className="column is-6"><span className="tag is-danger is-light is-large">{tranDau.team2}</span></div>
      <div className="column is-4"><input className="input block" type="number" placeholder="0" id={tranDau.id + 'team2'} disabled={true}></input></div>
      <div className="column"></div>
    </div>
    <div className="columns is-mobile has-text-centered">
      <div className="column"></div>
      <div className="column"><button className="button is-link is-small" disabled={true}>Đặt cược</button></div>
      <div className="column"></div>
    </div>
  </div>)
}

export function renderEnableBetMode1(tranDau) {

  if (tranDau.allowToBet) {
    return (
      <div>
        <div className="columns is-mobile has-text-centered">
          <div className="column"></div>
          <div className="column"><h4 className='subtitle is-4'>Dự đoán</h4></div>
          <div className="column"></div>
        </div>
        <div className="columns is-mobile has-text-centered">
          <div className="column"><span className="tag is-primary is-light is-large">{tranDau.team1}</span></div>
          <div className="column">
            <div className="control">
              <div className="select">
                <select id={tranDau.id + 'mode'}>
                  <option value={"-1"} defaultValue={true}>----------</option>
                  <option value={"0"}>Thang</option>
                  <option value={"1"}>Hoa</option>
                  <option value={"2"}>Thua</option>
                </select>
              </div>
            </div>
          </div>
          <div className="column"><span className="tag is-danger is-light is-large">{tranDau.team2}</span></div>
        </div>
        <div className="columns is-mobile has-text-centered">
          <div className="column"></div>
          <div className="column"><button className="button is-link is-small" onClick={() => onClickBet(tranDau.id, tranDau.mode)}>Đặt cược</button></div>
          <div className="column"></div>
        </div>
      </div>
    )
  }
  return (
    <div>
      <div className="columns is-mobile has-text-centered">
        <div className="column"></div>
        <div className="column"><h4 className='subtitle is-4'>Dự đoán</h4></div>
        <div className="column"></div>
      </div>
      <div className="columns is-mobile has-text-centered">
        <div className="column"><span className="tag is-primary is-light is-large">{tranDau.team1}</span></div>
        <div className="column">
          <div className="control">
            <div className="select">
              <select id="mode" disabled={true}>
                <option defaultValue={true}>----------</option>
                <option>Thang</option>
                <option>Hoa</option>
                <option>Thua</option>
              </select>
            </div>
          </div>
        </div>
        <div className="column"><span className="tag is-danger is-light is-large">{tranDau.team2}</span></div>
      </div>
      <div className="columns is-mobile has-text-centered">
        <div className="column"></div>
        <div className="column"><button className="button is-link is-small" disabled={true}>Đặt cược</button></div>
        <div className="column"></div>
      </div>
    </div>
  )
}

export function renderEnableBetMode2(tranDau) {

  if (tranDau.allowToBet) {
    return (
      <div>
        <div className="columns is-mobile has-text-centered">
          <div className="column"></div>
          <div className="column"><h4 className='subtitle is-4'>Dự đoán</h4></div>
          <div className="column"></div>
        </div>
        <div className="columns is-mobile has-text-centered">
          <div className="column"><span className="tag is-primary is-light is-large">{tranDau.team1}</span></div>
          <div className="column">
            <div className="control">
              <div className="select">
                <select id={tranDau.id + 'mode'}>
                  <option value={"-1"} defaultValue={true}>----------</option>
                  <option value={"0"}>Thang</option>
                  <option value={"1"}>Thua</option>
                </select>
              </div>
            </div>
          </div>
          <div className="column"><span className="tag is-danger is-light is-large">{tranDau.team2}</span></div>
        </div>
        <div className="columns is-mobile has-text-centered">
          <div className="column"></div>
          <div className="column"><button className="button is-link is-small" onClick={() => onClickBet(tranDau.id, tranDau.mode)}>Đặt cược</button></div>
          <div className="column"></div>
        </div>
      </div>
    )
  }
  return (
    <div>
      <div className="columns is-mobile has-text-centered">
        <div className="column"></div>
        <div className="column"><h4 className='subtitle is-4'>Dự đoán</h4></div>
        <div className="column"></div>
      </div>
      <div className="columns is-mobile has-text-centered">
        <div className="column"><span className="tag is-primary is-light is-large">{tranDau.team1}</span></div>
        <div className="column">
          <div className="control">
            <div className="select">
              <select id="mode" disabled={true}>
                <option defaultValue={true}>----------</option>
                <option>Thang</option>
                <option>Thua</option>
              </select>
            </div>
          </div>
        </div>
        <div className="column"><span className="tag is-danger is-light is-large">{tranDau.team2}</span></div>
      </div>
      <div className="columns is-mobile has-text-centered">
        <div className="column"></div>
        <div className="column"><button className="button is-link is-small" disabled={true}>Đặt cược</button></div>
        <div className="column"></div>
      </div>
    </div>

  )
}

export function renderDisableBet(tranDau, result) {

  if (tranDau.mode == 0) {
    return renderDisableBetMode0(tranDau, result);
  }
  if (tranDau.mode == 1) {
    return renderDisableBetMode1(tranDau, result);
  }
  if (tranDau.mode == 2) {
    return renderDisableBetMode2(tranDau, result);
  }
}

export function renderDisableBetMode0(tranDau, result) {
  const tmp = result.split("-");
  return (<div>
    <div className="columns is-mobile has-text-centered">
      <div className="column"></div>
      <div className="column"><h4 className='subtitle is-4'>Dự đoán của bạn</h4></div>
      <div className="column"></div>
    </div>
    <div className="columns is-mobile has-text-centered">
      <div className="column is-6"><span className="tag is-primary is-light is-large">{tranDau.team1}</span></div>
      <div className="column is-4"><input className="input block" type="number" placeholder="0" id={tranDau.id + 'team1'} disabled value={tmp[0]}></input></div>
      <div className="column"></div>
    </div>
    <div className="columns is-mobile has-text-centered">
      <div className="column is-6"><span className="tag is-danger is-light is-large">{tranDau.team2}</span></div>
      <div className="column is-4"><input className="input block" type="number" placeholder="0" id={tranDau.id + 'team2'} disabled value={tmp[1]}></input></div>
      <div className="column"></div>
    </div>
  </div>)
}

export function renderDisableBetMode1(tranDau, result) {

  return (
    <div>
      <div className="columns is-mobile has-text-centered">
        <div className="column"></div>
        <div className="column"><h4 className='subtitle is-4'>Dự đoán của bạn</h4></div>
        <div className="column"></div>
      </div>
      <div className="columns is-mobile has-text-centered">
        <div className="column"><span className="tag is-primary is-light is-large">{tranDau.team1}</span></div>
        <div className="column">
          <div className="control">
            <div className="select">
              <select id="mode" disabled={true}>
                <option selected={result === '0'}>Thang</option>
                <option selected={result === '1'}>Hoa</option>
                <option selected={result === '2'}>Thua</option>
              </select>
            </div>
          </div>
        </div>
        <div className="column"><span className="tag is-danger is-light is-large">{tranDau.team2}</span></div>
      </div>
    </div>
  )
}

export function renderDisableBetMode2(tranDau, result) {

  return (
    <div>
      <div className="columns is-mobile has-text-centered">
        <div className="column"></div>
        <div className="column"><h4 className='subtitle is-4'>Dự đoán của bạn</h4></div>
        <div className="column"></div>
      </div>
      <div className="columns is-mobile has-text-centered">
        <div className="column"><span className="tag is-primary is-light is-large">{tranDau.team1}</span></div>
        <div className="column">
          <div className="control">
            <div className="select">
              <select id="mode" disabled={true}>
                <option selected={result === '0'}>Thang</option>
                <option selected={result === '1'}>Thua</option>
              </select>
            </div>
          </div>
        </div>
        <div className="column"><span className="tag is-danger is-light is-large">{tranDau.team2}</span></div>
      </div>
    </div>

  )
}