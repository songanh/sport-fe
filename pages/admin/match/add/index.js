import Head from 'next/head';
import React, { useEffect } from 'react';
import Web3 from 'web3';
import { CONTRACT_APP_ADDRESS, CONTRACT_APP_ABI } from '../../../../utils/utils';

export default function AddMatch({ }) {

    useEffect(async () => {

        const web3 = new Web3(window.ethereum);
        web3.eth.getAccounts(function (err, accounts) {
            if (err != null) console.error("An error occurred: " + err);
            else if (accounts.length == 0) {
                window.location.href = '/';
            }
            else {
                const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
                appContract.methods.getEditor().call(function (err, res) {
                    if (err) {
                        console.log("An error occured", err)
                        return
                    }
                    if (res.toUpperCase() !== accounts[0].toUpperCase()) {
                        // Not admin redirect to Home
                        window.location.href = '/';
                    }
                });
            }
        });

        if (typeof window.ethereum !== 'undefined') {
            ethereum.on('accountsChanged', function (accounts) {
                if (accounts.length > 0) {
                    const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
                    appContract.methods.getEditor().call(function (err, res) {
                        if (err) {
                            console.log("An error occured", err)
                            return
                        }
                        if (res.toUpperCase() !== accounts[0].toUpperCase()) {
                            // Not admin redirect to Home
                            window.location.href = '/';
                        }
                    });
                } else {
                    window.location.href = '/';
                }
            });
        } else {
            //node = await renderMetaMaskInstall();
            // Not admin redirect to Home
            window.location.href = '/'
        }
    });

    return (
        <div>
            <Head>
                <title>Add new match</title>
                <meta name="description" content="Admin" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <main>
                <div>
                    <div className='block'></div>
                    <div className="columns">
                        <div className="column"></div>
                        <div className="column is-half">
                            <div className="box" id='admin'>
                                <h2 className='title is-2'>Add match info</h2>
                                <hr></hr>
                                <div className="field">
                                    <label className="label">Mode</label>
                                    <div className="control">
                                        <div className="select">
                                            <select id="mode">
                                                <option value={0}>Dự đoán tỷ số</option>
                                                <option value={1} defaultValue={true}>Dự đoán thắng hòa thua</option>
                                                <option value={2}>Dự đoán thắng thua</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <input className="input block" type="text" placeholder="Name ex: 90 mins" id='betName'></input>
                                <input className="input block" type="datetime-local" placeholder="Start Date" id='startDate'></input>
                                <input className="input block" type="text" placeholder="Vong dau" id='vongDau'></input>
                                <div className="field">
                                    <label className="label">Giải đấu</label>
                                    <div className="control">
                                        <div className="select">
                                            <select id="giaiDau">
                                                <option defaultValue={true}>Ngoại hạng Anh</option>
                                                <option>Champions League</option>
                                                <option>Europa League</option>
                                                <option>La Liga</option>
                                                <option>Serie A</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <input className="input block" type="text" placeholder="Team 1" id='team1'></input>
                                <input className="input block" type="text" placeholder="Team 2" id='team2'></input>
                                <input className="input block" type="number" placeholder="Bet price. ex 50 BET" id='betPrice'></input>
                                <input className="input block" type="number" placeholder="Fee" id='fee'></input>
                                <button className="button is-link is-small" onClick={addMatchInfo}>Add</button>
                            </div>
                        </div>
                        <div className="column"></div>
                    </div>
                </div>
            </main>

            <footer>

            </footer>
        </div>
    )
}

const addMatchInfo = async () => {

    const web3 = new Web3(window.ethereum);
    const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
    if (accounts.length > 0) {
        const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
        const startDate = new Date(document.getElementById('startDate').value).getTime();
        const vongDau = document.getElementById('vongDau').value;
        const giaiDau = document.getElementById('giaiDau').value;
        const team1 = document.getElementById('team1').value;
        const team2 = document.getElementById('team2').value;
        const betName = document.getElementById('betName').value;
        const betPrice = web3.utils.toWei(document.getElementById('betPrice').value, 'ether');
        const strId = team1 + team2 + startDate + betName + mode;
        strId = strId.toLowerCase();
        strId = strId.replace(/\s/g, '')
        const id = web3.utils.sha3(strId);
        const mode = parseInt(document.getElementById('mode').value);
        const fee = parseInt(document.getElementById('fee').value);

        appContract.methods
            .addMatchInfo([id, startDate, vongDau, giaiDau, team1, team2, false, true, betPrice, false, mode, "0-0", fee, betName])
            .send({}, function (err, res) {
                if (err) {
                    console.log("An error occured", err);
                    return;
                }
                alert("Add match info successfully!");
            });
    }
};



