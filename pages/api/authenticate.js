export default async function handler(req, res) {
    if (req.method === 'POST') {
        const response = await fetch(process.env.be + '/api/authenticate', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(req.body)
        });
        const data = await response.json();
        res.status(response.status).json(data)
    }
}