import Head from 'next/head';
import React, { useEffect } from 'react';
import Web3 from 'web3';
import { CONTRACT_APP_ADDRESS, CONTRACT_APP_ABI, renderTo } from '../../../utils/utils';

export default function Match({ }) {

    useEffect(async () => {

        const web3 = new Web3(window.ethereum);
        web3.eth.getAccounts(function (err, accounts) {
            if (err != null) console.error("An error occurred: " + err);
            else if (accounts.length == 0) {
                window.location.href = '/';
            }
            else {
                const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
                appContract.methods.getEditor().call(function (err, res) {
                    if (err) {
                        console.log("An error occured", err)
                        return
                    }
                    if (res.toUpperCase() !== accounts[0].toUpperCase()) {
                        // Not admin redirect to Home
                        window.location.href = '/';
                    }
                });

                appContract.methods.getMatch(true).call(function (err, res) {
                    if (err) {
                        console.log("An error occured", err)
                        return
                    }
                    const muccuoc = {};
                    if (res.length > 0) {
                        const lichthidau = [];
                        for (const match of res) {
                            lichthidau.push({ 'id': match[0], 'team1': match[4], 'team2': match[5], 'tengiaidau': match[3], 'vongdau': match[2], 'startDate': parseInt(match[1]), 'allowToBet': match[7], 'mode': parseInt(match[10]) });
                            muccuoc[match[0]] = web3.utils.fromWei(match[8], 'ether');
                        }
                        sessionStorage.setItem('muccuoc', JSON.stringify(muccuoc));
                        let scheduleNode = renderLichThiDau(lichthidau);
                        renderTo(scheduleNode, 'schedule');
                    }
                });
            }
        });

        if (typeof window.ethereum !== 'undefined') {
            ethereum.on('accountsChanged', function (accounts) {
                if (accounts.length > 0) {
                    const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
                    appContract.methods.getEditor().call(function (err, res) {
                        if (err) {
                            console.log("An error occured", err)
                            return
                        }
                        if (res.toUpperCase() !== accounts[0].toUpperCase()) {
                            // Not admin redirect to Home
                            window.location.href = '/';
                        }
                    });
                } else {
                    window.location.href = '/';
                }
            });
        } else {
            //node = await renderMetaMaskInstall();
            // Not admin redirect to Home
            window.location.href = '/'
        }
    });

    return (
        <div>
            <Head>
                <title>Match List</title>
                <meta name="description" content="Admin" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <main>
                <div id='wallet'>

                </div>
                <div id='schedule'>
                </div>
            </main>

            <footer>

            </footer>
        </div>
    )
}

export function renderLichThiDau(data) {

    return (<div>
        {
            data.map((row) => {
                const id = row.id;
                const team1 = row.team1;
                const team2 = row.team2;
                const giaiDau = row.tengiaidau;
                const vongDau = row.vongdau;
                const allowToBet = row.allowToBet;
                const startDate = new Date(row.startDate);
                const strDate = ((startDate.getDate() > 9) ? startDate.getDate() : ('0' + startDate.getDate())) + '/' + ((startDate.getMonth() > 8) ? (startDate.getMonth() + 1) : ('0' + (startDate.getMonth() + 1))) + '/' + startDate.getFullYear();
                const strTime = ((startDate.getHours() > 9) ? startDate.getHours() : ('0' + startDate.getHours())) + ':' + ((startDate.getMinutes() > 9) ? startDate.getMinutes() : ('0' + startDate.getMinutes()));
                const tranDau = { 'id': id, 'team1': team1, 'team2': team2, 'tenGiaiDau': giaiDau, 'strDate': strDate, 'strTime': strTime, 'vongDau': vongDau, 'allowToBet': allowToBet, 'mode': row.mode };
                return renderTranDau(tranDau)
            })
        }
    </div>)
}

export function renderTranDau(tranDau) {
    return (<div key={tranDau.id}>
        <div className='block'></div>
        <div className="columns">
            <div className="column"></div>
            <div className="column is-half">
                <div className="box">
                    {/* <h3 className='subtitle is-3'>19/2/2022</h3> */}
                    <div className="columns is-mobile has-text-centered">
                        <div className="column"><b>{tranDau.strDate}</b></div>
                        <div className="column"><b>{tranDau.vongDau}</b></div>
                        <div className="column"><b>{tranDau.tenGiaiDau}</b></div>
                    </div>
                    <hr></hr>
                    <div className="columns is-mobile has-text-centered">
                        <div className="column"><span className="tag is-primary is-light is-large">{tranDau.team1}</span></div>
                        <div className="column">{tranDau.strTime}</div>
                        <div className="column"><span className="tag is-danger is-light is-large">{tranDau.team2}</span></div>
                    </div>
                    <hr></hr>
                    <div className="columns is-mobile">
                        <div className="column"><b>Disable bet</b></div>
                        <div className="column"><input type="checkbox" checked={!tranDau.allowToBet} disabled={!tranDau.allowToBet} onChange={() => disableBet(tranDau.id)} /></div>
                        <div className="column"></div>
                    </div>
                    <hr></hr>
                    {renderResultMode(tranDau.id, tranDau.mode)}
                    <div className="column"><button className="button is-link is-small" onClick={() => updateScore(tranDau.id, tranDau.mode)}>Update Score</button></div>
                    <hr></hr>
                    <div className="columns is-mobile">
                        <div className="column"><b>Finish bet</b></div>
                        <div className="column"><input type="checkbox" onChange={() => onClickFinishMatch(tranDau.id)} /></div>
                        <div className="column"></div>
                    </div>
                </div>
            </div>
            <div className="column"></div>
        </div>
    </div>)
}

const updateScore = async (updateScoreId, mode) => {

    const web3 = new Web3(window.ethereum);
    const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
    if (accounts.length > 0) {
        const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
        if (mode == 0) {

            const goal1 = document.getElementById(updateScoreId + 'goal1').value;
            const goal2 = document.getElementById(updateScoreId + 'goal2').value;
            const result = goal1 + '-' + goal2;
            if (result) {
                appContract.methods
                    .updateResult(updateScoreId, result)
                    .send({}, function (err, res) {
                        if (err) {
                            console.log("An error occured", err);
                            return;
                        }
                        alert("Update score successfully!");
                    });
            } else {
                alert("Ty so khong hop le!");
            }
        } else {
            const result = document.getElementById(updateScoreId + 'mode').value;
            if (result) {
                appContract.methods
                    .updateResult(updateScoreId, result)
                    .send({}, function (err, res) {
                        if (err) {
                            console.log("An error occured", err);
                            return;
                        }
                        alert("Update score successfully!");
                    });
            } else {
                alert("Ket qua khong hop le!");
            }
        }
    }
};

const disableBet = async (disableId) => {

    const web3 = new Web3(window.ethereum);
    const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: window.ethereum.selectedAddress });
    appContract.methods
        .disableBet(disableId)
        .send({}, function (err, res) {
            if (err) {
                console.log("An error occured", err);
                return;
            }
            alert("Disable bet successfully!");
            location.reload();
        });
};

const onClickFinishMatch = async (matchId) => {

    const web3 = new Web3(window.ethereum);
    const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: window.ethereum.selectedAddress });
    if (matchId) {
        appContract.methods
            .finishMatch(matchId)
            .send({}, function (err, res) {
                if (err) {
                    console.log("An error occured", err);
                    return;
                }
                alert("Finish match successfully!");
                location.reload();
            });
    }

};

export function renderResultMode(id, mode) {

    if (mode == 1) {
        return (<div className="field">
            <label className="label">Update result</label>
            <div className="control">
                <div className="select">
                    <select id={id + 'mode'}>
                        <option value={"0"}>Thắng</option>
                        <option value={"1"}>Hòa</option>
                        <option value={"2"}>Thua</option>
                    </select>
                </div>
            </div>
        </div>)
    }
    if (mode == 2) {
        return (<div className="field">
            <label className="label">Update result</label>
            <div className="control">
                <div className="select">
                    <select id={id + 'mode'}>
                        <option value={"0"}>Thắng</option>
                        <option value={"1"}>Thua</option>
                    </select>
                </div>
            </div>
        </div>)
    }
    return (<div className="columns is-mobile">
        <div className="column"><b>Update score</b></div>
        <div className="column"><input className="input block" type="number" placeholder="Goal 1" id={id + 'goal1'}></input></div>
        <div className="column"><input className="input block" type="number" placeholder="Goal 2" id={id + 'goal2'}></input></div>
    </div>)
}


