import Head from 'next/head';
import React, { useEffect } from 'react';
import Link from 'next/link'
import Web3 from 'web3';
import { CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, renderBalance, renderTo, renderMetaMaskInstall } from '../../utils/utils';

export default function Swap({ }) {

    useEffect(async () => {

        const web3 = new Web3(window.ethereum);
        web3.eth.getAccounts(function (err, accounts) {
            if (err != null) console.error("An error occurred: " + err);
            else if (accounts.length == 0) {
                const node = renderBalance('', -1);
                renderTo(node, 'wallet');
            }
            else {
                const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
                appContract.methods.balanceOf(accounts[0]).call(function (err, res) {
                    if (err) {
                        console.log("An error occured", err)
                        return
                    }
                    const node = renderBalance(accounts[0], web3.utils.fromWei(res, 'ether'));
                    renderTo(node, 'wallet');
                });
            }
        });
        if (typeof window.ethereum !== 'undefined') {
            ethereum.on('accountsChanged', function (accounts) {
                if (accounts.length > 0) {
                    const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
                    appContract.methods.balanceOf(accounts[0]).call(function (err, res) {
                        if (err) {
                            console.log("An error occured", err)
                            return
                        }
                        const node = renderBalance(accounts[0], web3.utils.fromWei(res, 'ether'));
                        renderTo(node, 'wallet');
                    });
                } else {
                    const node = renderBalance('', -1);
                    renderTo(node, 'wallet');
                }
            });
        } else {
            const node = renderMetaMaskInstall();
            renderTo(node, 'wallet');
        }
    });

    return (
        <div>
            <Head>
                <title>Swap BNB to BET</title>
                <meta name="description" content="Swap BNB to BET" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <main>
                <div className='block'></div>
                <div className="columns is-mobile has-text-centered">
                    <div className="column"></div>
                    <div className="column is-10">
                        <nav className="breadcrumb" aria-label="breadcrumbs">
                            <ul>
                                <li><Link href="/"><a>Home</a></Link></li>
                                <li className="is-active"><Link href="/swap"><a>Swap</a></Link></li>
                                <li><Link href="https://docs.binance.org/smart-chain/wallet/metamask.html" target="_blank"><a>Setup BSC Test Net</a></Link></li>
                                <li><Link href="https://testnet.binance.org/faucet-smart" target="_blank"><a>Faucet</a></Link></li>
                            </ul>
                        </nav>
                    </div>
                    <div className="column"></div>
                </div>
                <div id='wallet'>

                </div>
                <div>
                    <div className='block'></div>
                    <div className="box match-width" id='admin'>
                        <h2 className='title is-2'>Buy</h2>
                        <hr></hr>
                        <input className="input block" type="number" placeholder="0.01" id='buy' min={0} step={0.01}></input>
                        <button className="button is-link is-small" onClick={onClickBuy}>Buy</button>
                    </div>
                </div>
                <div>
                    <div className='block'></div>
                    <div className="box match-width" id='admin'>
                        <h2 className='title is-2'>Sell</h2>
                        <hr></hr>
                        <input className="input block" type="number" placeholder="100" id='sell' min={0} step={1}></input>
                        <button className="button is-link is-small" onClick={onClickSell}>Sell</button>
                    </div>
                </div>
                <div id="snackbar">Address coppied to clipboard!</div>
            </main>

            <footer>

            </footer>
        </div>
    )
}

const onClickBuy = async () => {

    const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
    if (accounts.length > 0) {
        const web3 = new Web3(window.ethereum);
        const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
        const amount = document.getElementById('buy').value;
        if (parseFloat(amount) > 0) {
            appContract.methods
                .buyTokens()
                .send({ 'value': web3.utils.toWei(amount, 'ether') }, function (err, res) {
                    if (err) {
                        console.log("An error occured", err);
                        return;
                    }
                }).then(function (receipt) {
                    // will be fired once the receipt is mined
                    appContract.methods.balanceOf(accounts[0]).call(function (err, res) {
                        if (err) {
                            console.log("An error occured", err)
                            return
                        }
                        const node = renderBalance(accounts[0], web3.utils.fromWei(res, 'ether'));
                        renderTo(node, 'wallet');
                    });
                });
        }
    }
};

const onClickSell = async () => {

    const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
    if (accounts.length > 0) {
        const web3 = new Web3(window.ethereum);
        const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
        appContract.methods.balanceOf(accounts[0]).call(function (err, res) {
            if (err) {
                console.log("An error occured", err)
                return
            }
            const lastestBalance = web3.utils.fromWei(res, 'ether')
            const node = renderBalance(accounts[0], lastestBalance);
            renderTo(node, 'wallet');
            const amount = document.getElementById('sell').value;
            if (parseFloat(amount) > 0 && lastestBalance >= parseFloat(amount)) {
                appContract.methods
                    .sellTokens(web3.utils.toWei(amount, 'ether'))
                    .send({}, function (err, res) {
                        if (err) {
                            console.log("An error occured", err);
                            return;
                        }
                    })
                    .then(function (receipt) {
                        // will be fired once the receipt is mined
                        appContract.methods.balanceOf(accounts[0]).call(function (err, res) {
                            if (err) {
                                console.log("An error occured", err)
                                return
                            }
                            const node = renderBalance(accounts[0], web3.utils.fromWei(res, 'ether'));
                            renderTo(node, 'wallet');
                        });
                    })
            } else {
                alert('So du cua ban khong du!');
            }
        });
    }
};