import ReactDOM from 'react-dom';

export const CONTRACT_APP_ADDRESS = '0xc4C9134813b6c20331901C627759dDf684606c4f';

export const CONTRACT_APP_ABI = [
  {
    "inputs": [],
    "stateMutability": "nonpayable",
    "type": "constructor"
  },
  {
    "inputs": [],
    "name": "getOwner",
    "outputs": [
      {
        "internalType": "address",
        "name": "",
        "type": "address"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [],
    "name": "decimals",
    "outputs": [
      {
        "internalType": "uint8",
        "name": "",
        "type": "uint8"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [],
    "name": "symbol",
    "outputs": [
      {
        "internalType": "string",
        "name": "",
        "type": "string"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [],
    "name": "name",
    "outputs": [
      {
        "internalType": "string",
        "name": "",
        "type": "string"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [],
    "name": "totalSupply",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "account",
        "type": "address"
      }
    ],
    "name": "balanceOf",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [],
    "name": "buyTokens",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function",
    "payable": true
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_amount",
        "type": "uint256"
      }
    ],
    "name": "sellTokens",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "newOwner",
        "type": "address"
      }
    ],
    "name": "changeOwner",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "newEditor",
        "type": "address"
      }
    ],
    "name": "changeEditor",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "newFeeHolder",
        "type": "address"
      }
    ],
    "name": "changeFeeHolder",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "getEditor",
    "outputs": [
      {
        "internalType": "address",
        "name": "",
        "type": "address"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [
      {
        "components": [
          {
            "internalType": "string",
            "name": "id",
            "type": "string"
          },
          {
            "internalType": "uint256",
            "name": "startDate",
            "type": "uint256"
          },
          {
            "internalType": "string",
            "name": "vongdau",
            "type": "string"
          },
          {
            "internalType": "string",
            "name": "giaidau",
            "type": "string"
          },
          {
            "internalType": "string",
            "name": "team1",
            "type": "string"
          },
          {
            "internalType": "string",
            "name": "team2",
            "type": "string"
          },
          {
            "internalType": "bool",
            "name": "finish",
            "type": "bool"
          },
          {
            "internalType": "bool",
            "name": "allowBet",
            "type": "bool"
          },
          {
            "internalType": "uint256",
            "name": "betPrice",
            "type": "uint256"
          },
          {
            "internalType": "bool",
            "name": "cancel",
            "type": "bool"
          },
          {
            "internalType": "uint8",
            "name": "mode",
            "type": "uint8"
          },
          {
            "internalType": "string",
            "name": "result",
            "type": "string"
          },
          {
            "internalType": "uint8",
            "name": "fee",
            "type": "uint8"
          },
          {
            "internalType": "string",
            "name": "name",
            "type": "string"
          }
        ],
        "internalType": "struct App.MatchInfo",
        "name": "matchInfo",
        "type": "tuple"
      }
    ],
    "name": "addMatchInfo",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "string",
        "name": "id",
        "type": "string"
      },
      {
        "internalType": "string",
        "name": "result",
        "type": "string"
      }
    ],
    "name": "updateResult",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "string",
        "name": "id",
        "type": "string"
      }
    ],
    "name": "disableBet",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "string",
        "name": "id",
        "type": "string"
      }
    ],
    "name": "cancelBet",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "string",
        "name": "id",
        "type": "string"
      }
    ],
    "name": "finishMatch",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "string",
        "name": "id",
        "type": "string"
      }
    ],
    "name": "getBet",
    "outputs": [
      {
        "components": [
          {
            "internalType": "string",
            "name": "matchId",
            "type": "string"
          },
          {
            "internalType": "address",
            "name": "player",
            "type": "address"
          },
          {
            "internalType": "string",
            "name": "bet",
            "type": "string"
          }
        ],
        "internalType": "struct App.BetInfo",
        "name": "response",
        "type": "tuple"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [
      {
        "internalType": "string",
        "name": "id",
        "type": "string"
      }
    ],
    "name": "getBetList",
    "outputs": [
      {
        "components": [
          {
            "internalType": "string",
            "name": "matchId",
            "type": "string"
          },
          {
            "internalType": "address",
            "name": "player",
            "type": "address"
          },
          {
            "internalType": "string",
            "name": "bet",
            "type": "string"
          }
        ],
        "internalType": "struct App.BetInfo[]",
        "name": "",
        "type": "tuple[]"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [
      {
        "internalType": "string",
        "name": "id",
        "type": "string"
      }
    ],
    "name": "getMatch",
    "outputs": [
      {
        "components": [
          {
            "internalType": "string",
            "name": "id",
            "type": "string"
          },
          {
            "internalType": "uint256",
            "name": "startDate",
            "type": "uint256"
          },
          {
            "internalType": "string",
            "name": "vongdau",
            "type": "string"
          },
          {
            "internalType": "string",
            "name": "giaidau",
            "type": "string"
          },
          {
            "internalType": "string",
            "name": "team1",
            "type": "string"
          },
          {
            "internalType": "string",
            "name": "team2",
            "type": "string"
          },
          {
            "internalType": "bool",
            "name": "finish",
            "type": "bool"
          },
          {
            "internalType": "bool",
            "name": "allowBet",
            "type": "bool"
          },
          {
            "internalType": "uint256",
            "name": "betPrice",
            "type": "uint256"
          },
          {
            "internalType": "bool",
            "name": "cancel",
            "type": "bool"
          },
          {
            "internalType": "uint8",
            "name": "mode",
            "type": "uint8"
          },
          {
            "internalType": "string",
            "name": "result",
            "type": "string"
          },
          {
            "internalType": "uint8",
            "name": "fee",
            "type": "uint8"
          },
          {
            "internalType": "string",
            "name": "name",
            "type": "string"
          }
        ],
        "internalType": "struct App.MatchInfo",
        "name": "",
        "type": "tuple"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [
      {
        "internalType": "bool",
        "name": "active",
        "type": "bool"
      }
    ],
    "name": "getMatch",
    "outputs": [
      {
        "components": [
          {
            "internalType": "string",
            "name": "id",
            "type": "string"
          },
          {
            "internalType": "uint256",
            "name": "startDate",
            "type": "uint256"
          },
          {
            "internalType": "string",
            "name": "vongdau",
            "type": "string"
          },
          {
            "internalType": "string",
            "name": "giaidau",
            "type": "string"
          },
          {
            "internalType": "string",
            "name": "team1",
            "type": "string"
          },
          {
            "internalType": "string",
            "name": "team2",
            "type": "string"
          },
          {
            "internalType": "bool",
            "name": "finish",
            "type": "bool"
          },
          {
            "internalType": "bool",
            "name": "allowBet",
            "type": "bool"
          },
          {
            "internalType": "uint256",
            "name": "betPrice",
            "type": "uint256"
          },
          {
            "internalType": "bool",
            "name": "cancel",
            "type": "bool"
          },
          {
            "internalType": "uint8",
            "name": "mode",
            "type": "uint8"
          },
          {
            "internalType": "string",
            "name": "result",
            "type": "string"
          },
          {
            "internalType": "uint8",
            "name": "fee",
            "type": "uint8"
          },
          {
            "internalType": "string",
            "name": "name",
            "type": "string"
          }
        ],
        "internalType": "struct App.MatchInfo[]",
        "name": "",
        "type": "tuple[]"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [
      {
        "internalType": "string",
        "name": "matchId",
        "type": "string"
      },
      {
        "internalType": "string",
        "name": "playerBet",
        "type": "string"
      }
    ],
    "name": "bet",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  }
];

export async function renderMetaMaskConnect() {
  return (<div>
    <div className='block'></div>
    <div className="columns">
      <div className="column"></div>
      <div className="column match-width">
        <div className="box">
          <h3 className='title is-3'>Connect to MetaMask wallet</h3>
          <hr></hr>
          <div className="has-text-centered" id='btnConnect'>
            <button className="button is-warning" onClick={onClickConnect}>Connect</button>
          </div>
        </div>
      </div>
      <div className="column"></div>
    </div>
  </div>)
}

const onClickConnect = async () => {
  try {
    // Will open the MetaMask UI
    // You should disable this button while the request is pending!
    await ethereum.request({ method: 'eth_requestAccounts' });
  } catch (error) {
    let node = await renderConnectError();
    renderTo(node, 'btnConnect');
  }
};

export function renderMetaMaskInstall() {
  return (<div>
    <div className='block'></div>
    <div className="box match-width">
      <h3 className='title is-3'>Install MetaMask to start ...</h3>
      <hr></hr>
      <div className="has-text-centered">
        <a href='https://metamask.io/download.html'>
          <button className="button is-warning">Install</button>
        </a>
      </div>
    </div>
  </div>)
}


export function renderBalance(account, balance) {
  if (account && balance && balance >= 0) {
    return (<div>
      <div className='block'></div>
      <div className="box match-width">
        <p className='txt-truncate'>Account: <a onClick={coppy}><b>{window.ethereum.selectedAddress}</b></a></p>
        <hr></hr>
        <div className="has-text-centered">
          <h3 className='title is-3'>{parseFloat(balance).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + ' BET'}</h3>
        </div>
      </div>
    </div>)
  }

  return (<div>
    <div className='block'></div>
    <div className="box match-width">
      <p>{'Account:'}</p>
      <hr></hr>
      <a onClick={connect}>
        <div className="notification is-danger">
          Please OPEN and UNLOCK your MetaMask wallet!
        </div>
      </a>
    </div>
  </div>)
}

export async function renderConnectError() {
  return (<div>
    <div className="notification is-danger">
      Please OPEN and UNLOCK your MetaMask wallet!
    </div>
  </div>)
}

export function renderTo(node, id) {

  let div = document.createElement('div');
  ReactDOM.render(node, div);
  const element = document.getElementById(id);
  element.innerHTML = '';
  element.appendChild(div);
}

const connect = async () => {
  await window.ethereum.request({ method: 'eth_requestAccounts' });
}

const coppy = () => {
  navigator.clipboard.writeText(window.ethereum.selectedAddress);
  var x = document.getElementById("snackbar");
  x.className = "show";
  setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
}