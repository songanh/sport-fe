import Head from 'next/head';
import React, { useEffect } from 'react';
import Web3 from 'web3';
import { CONTRACT_APP_ADDRESS, CONTRACT_APP_ABI } from '../../utils/utils';

export default function Admin({ }) {

    useEffect(async () => {

        const web3 = new Web3(window.ethereum);
        web3.eth.getAccounts(function (err, accounts) {
            if (err != null) console.error("An error occurred: " + err);
            else if (accounts.length == 0) {
                window.location.href = '/';
            }
            else {
                const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
                appContract.methods.getOwner().call(function (err, res) {
                    if (err) {
                        console.log("An error occured", err)
                        return
                    }
                    if (res.toUpperCase() !== accounts[0].toUpperCase()) {
                        // Not admin redirect to Home
                        window.location.href = '/'
                    }
                });
            }
        });
        if (typeof window.ethereum !== 'undefined') {
            ethereum.on('accountsChanged', function (accounts) {
                if (accounts.length > 0) {
                    const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
                    appContract.methods.getEditor().call(function (err, res) {
                        if (err) {
                            console.log("An error occured", err)
                            return
                        }
                        if (res.toUpperCase() !== accounts[0].toUpperCase()) {
                            // Not admin redirect to Home
                            window.location.href = '/';
                        }
                    });
                } else {
                    window.location.href = '/';
                }
            });
        } else {
            //node = await renderMetaMaskInstall();
            // Not admin redirect to Home
            window.location.href = '/'
        }
    });

    return (
        <div>
            <Head>
                <title>Admin</title>
                <meta name="description" content="Admin" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <main>
                <div id='wallet'>

                </div>
                <div>
                    <div className='block'></div>
                    <div className="columns">
                        <div className="column"></div>
                        <div className="column is-half">
                            <div className="box" id='admin'>
                                <h2 className='title is-2'>Change fee holder</h2>
                                <hr></hr>
                                <input className="input block" type="text" placeholder="0x..." id='holder'></input>
                                <button className="button is-link is-small" onClick={onClickUpdateFeeHolder}>Update</button>
                            </div>
                        </div>
                        <div className="column"></div>
                    </div>
                </div>
                <div>
                    <div className='block'></div>
                    <div className="columns">
                        <div className="column"></div>
                        <div className="column is-half">
                            <div className="box" id='admin'>
                                <h2 className='title is-2'>Change Editor</h2>
                                <hr></hr>
                                <input className="input block" type="text" placeholder="0x..." id='editor'></input>
                                <button className="button is-link is-small" onClick={onClickChangeEditor}>Update</button>
                            </div>
                        </div>
                        <div className="column"></div>
                    </div>
                </div>
            </main>

            <footer>

            </footer>
        </div>
    )
}

const onClickUpdateFeeHolder = async () => {

    const web3 = new Web3(window.ethereum);
    const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
    if (accounts.length > 0) {
        const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
        const holder = document.getElementById('holder').value;
        if (holder) {
            appContract.methods
                .changeFeeHolder(holder)
                .send({}, function (err, res) {
                    if (err) {
                        console.log("An error occured", err);
                        return;
                    }
                    alert("Update holder successfully!");
                    location.reload();
                });
        }
    }
};

const onClickChangeEditor = async () => {

    const web3 = new Web3(window.ethereum);
    const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
    if (accounts.length > 0) {
        const appContract = new web3.eth.Contract(CONTRACT_APP_ABI, CONTRACT_APP_ADDRESS, { from: accounts[0] });
        const editor = document.getElementById('editor').value;
        if (editor) {
            appContract.methods
                .changeEditor(editor)
                .send({}, function (err, res) {
                    if (err) {
                        console.log("An error occured", err);
                        return;
                    }
                    alert("Update holder successfully!");
                    location.reload();
                });
        }
    }
};

